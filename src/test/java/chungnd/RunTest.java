package chungnd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "chungnd\\steps",
        features = "src\\test\\resources\\features",
//        plugin = {"json:target/report/cucumber.json"}
//        plugin = {"pretty","html:target/report"}
        plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"}
)
public class RunTest {
}
