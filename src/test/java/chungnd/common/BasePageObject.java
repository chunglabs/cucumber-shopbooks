package chungnd.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePageObject extends PageFactory {
    public WebDriver driver;
    Actions action;
    WebDriverWait wait;

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void clickOn(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void doubleClickOn(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        action.doubleClick(element);
    }


}
