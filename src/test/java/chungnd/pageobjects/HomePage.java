package chungnd.pageobjects;

import chungnd.common.BasePageObject;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePageObject {
    public HomePage(WebDriver driver) {
        super(driver);
    }
    public void open(){
        driver.get("https://gitlab.com");
    }
}
