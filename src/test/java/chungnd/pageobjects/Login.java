package chungnd.pageobjects;

import chungnd.common.BasePageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login extends BasePageObject {
    public Login(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "")
    private WebElement USERNAME_FIELD;
    @FindBy(id = "")
    private WebElement PASSWORD_FIELD;
    @FindBy(css = "")
    private WebElement REMEMBERME_CHECKBOX;
    @FindBy(css = "")
    private WebElement LOGIN_BTN;

    public Login with(String username){
        USERNAME_FIELD.sendKeys(username);
        return this;
    }
    public Login and(String password){
        PASSWORD_FIELD.sendKeys(password);
        return this;
    }
    public Login clickLogin(){
        clickOn(LOGIN_BTN);
        return this;
    }
}
