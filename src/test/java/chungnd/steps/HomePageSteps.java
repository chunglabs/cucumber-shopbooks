package chungnd.steps;

import chungnd.pageobjects.HomePage;
import chungnd.pageobjects.Login;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;

public class HomePageSteps {
    WebDriver driver;
    HomePage homePage;

    public HomePageSteps() {
        driver = Hooks.driver;
        homePage = new HomePage(driver);
    }

    @Given("^that Leo opened the Home page$")
    public void open_the_home_page(){
        homePage.open();
    }
    @Then("^the Home page must contains only (.*) sliders$")
    public void the_home_page_must_contains_only_number_sliders(int numOfSliders){

    }
}
